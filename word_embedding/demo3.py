from gensim import corpora
from gensim.test.utils import common_texts,datapath
from gensim.models import Word2Vec,word2vec,KeyedVectors
import jieba
import os

#https://radimrehurek.com/gensim_3.8.3/models/keyedvectors.html#module-gensim.models.keyedvectors

def zhuxian_tokenize():
	'''
	针对诛仙进行分词,生成预料库
	'''
	with open('./诛仙.txt', 'r') as f:
		content = f.read()
	#简单用逗号隔开
	content = content.replace("？", "。");
	content = content.replace("！", "。");
	documents = content.split("。")

	texts = ""
	for w in documents:
		#word2vec.LineSentence格式要求，一行一句话，中间有空格隔开
		texts += " ".join(list(jieba.cut(w))) + "\n"

	# texts = texts.encode('utf-8')
	with open("./诛仙.cor", "w") as f:
		f.write(texts)

def word2vec_test():
	if not os.path.exists("./诛仙.model"):
		zhuxian_tokenize()
		sentences = word2vec.LineSentence("./诛仙.cor")
		model = Word2Vec(sentences, size=100, window=5, min_count=1, workers=4)
		model.save("./诛仙.model")
	else:
		model = Word2Vec.load("./诛仙.model")

	if not os.path.exists("./诛仙.kv"):
		model.wv.save("./诛仙.kv")

	word_vectors = KeyedVectors.load('./诛仙.kv', mmap='r')
	result = word_vectors.most_similar(positive=['张小凡', '青云'], negative=['鬼厉'])
	print("{}: {:.4f}".format(*result[0]))

	result = word_vectors.most_similar_cosmul(positive=['张小凡', '鬼王'], negative=['陆雪琪'])
	print("{}: {:.4f}".format(*result[0]))

	result = word_vectors.similar_by_word("兽神")
	print("{}: {:.4f}".format(*result[0]))

	#输出张小凡的词向量
	print(word_vectors['张小凡'])


def chinese():
	'''
	jieba分词例子
	'''
	documents = ['工业互联网平台的核心技术是什么', '工业现场生产过程优化场景有哪些']

	def word_cut(doc):
		seg = [list(jieba.cut(w)) for w in documents]
		return seg
	#分词
	texts = word_cut(documents)
	print(texts)
	exit(0)
	print(texts)
	#分词编码
	dictionary = corpora.Dictionary(texts)
	#把文档描述成向量((0,1)) 元组第一项表示词编码中的编号，第二项表示出现的次数
	bow_corpus = [dictionary.doc2bow(text) for text in texts]

	print(bow_corpus)

def demo():
	'''
	官网gensim word2vec例子
	'''
	model = Word2Vec(sentences=common_texts, size=100, window=5, min_count=1, workers=4)
	model.save("word2vec.model")
	model = Word2Vec.load("word2vec.model")
	model.train([["hello", "world"]], total_examples=1, epochs=1)
	vector = model.wv['computer'] 
	print(vector)
	# sims = model.wv.most_similar('computer', topn=10)
	# print(sims)

if __name__ == '__main__':
	word2vec_test()
