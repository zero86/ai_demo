# import nltk
# nltk.download('punkt')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.layers import Flatten, Dense, Embedding
import numpy as np

def get_content():
	with open("./liudehua.txt", "r") as f:
		content = f.read()
	#过滤特殊标记
	content = content.replace('/t', '')
	content = content.replace('/n', '')
	content = content.replace(',', '')
	content = content.replace('``', '')
	content = content.replace(':', '')
	content = content.replace(';', '')
	content = content.replace('.', '')
	content = content.replace("'", '')
	content = content.replace("''", '')
	content = content.replace("-", '')
	#分词
	text_st = word_tokenize(content)
	#过滤删除停止词
	st = set(stopwords.words("english"))
	tokenize_origin = []
	for v in text_st:
		if v not in st:
			tokenize_origin.append(v)
	text_st = set(tokenize_origin)

	#过滤化分词
	tokenize = set(text_st) - st

	#建立词典索引{"词":"编号"}
	word_dict = {w:i for i, w in enumerate(tokenize)}

	#建立索引词典{"编号":"词"}
	index_dict = {i:w for i, w in enumerate(tokenize)}
	return word_dict, index_dict, tokenize, tokenize_origin

def get_train_data(window_length=10):
	word_dict, index_dict, tokenize,tokenize_origin = get_content()
	tokenize = list(tokenize)
	x_train = []
	y_train = []
	for i in range(window_length, len(tokenize_origin)):
		temp = []
		for k,v in enumerate(tokenize_origin[i-window_length:i]):
			temp.append((word_dict[v]))
		y_train.append(word_dict[tokenize_origin[i]])
		x_train.append(temp)
	return x_train, y_train, len(word_dict),index_dict

class model_nnlm(Model):
	def __init__(self, dict_len, window_length):
		super(model_nnlm,self).__init__()
		self.e1 = Embedding(dict_len, window_length)
		self.flatten = Flatten()
		#隐藏层
		self.hidden = Dense(100, activation=tf.keras.activations.tanh)
		#输出层
		self.out = Dense(dict_len, activation='softmax')

	def call(self, x):
		x = self.e1(x)
		x = self.flatten(x)
		x = self.hidden(x)
		x = self.out(x)
		return x

def train():
	window_length = 10
	x_train, y_train, dict_len,index_dict = get_train_data(window_length)
	#初始化模型
	m_model = model_nnlm(dict_len, window_length)

	#配置训练方法
	m_model.compile(optimizer=tf.keras.optimizers.Adam(0.001),
		loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=False),
		metrics=['sparse_categorical_accuracy'])
	#训练网络
	m_model.fit(np.array(x_train), np.array(y_train), batch_size=6, epochs=100)

	# 打印网络结构
	# m_model.summary()
	#获取模型参数
	print(m_model.trainable_variables)

	#预测
	result = m_model.predict(np.array(x_train))
	result = np.argmax(result, axis=1)
	print(result)
	print(y_train)


if __name__ == "__main__":
	train()